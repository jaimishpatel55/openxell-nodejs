const express = require('express')
const mongoose = require('mongoose')
const databaseconfig = require('./db/db');
const userRouter = require('./routes/user')

const app = express();
app.use(express.json())

mongoose.connect(databaseconfig.DB, { useNewUrlParser: true,useUnifiedTopology: true }).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database'+ err)}
);

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-auth-token');
    res.setHeader('Content-Type', 'application/json');

    if (req.method === 'OPTIONS') {
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        return res.status(200).json({});
    }

    next();
});


app.use('/api/user',userRouter)

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));