const mongoose = require('mongoose')

var UserSchema = new mongoose.Schema({
    Email:{
        type: String,
        required:true
    },
    Password:{
        type: String,
        required:true
    },
    Username:{
        type: String,
        required:true
    }
}, {timestamps: true});
  
module.exports = mongoose.model('User', UserSchema);