const express = require('express');
const router = express.Router();
const userService = require('../services/user-service')
const Constants = require('../services/constants')
const bcrypt = require('bcrypt');
const atob = require('atob');
const utilityService = require('../services/utility-service')

router.post('/register',async (req,res) =>{
    const userExists = await userService.userExists(req.body.Email);
    if (userExists) return res.status(400).send({ errorMessage: Constants.userExistsError });

    const user = await userService.registerUser(req.body.Email, req.body.Username, atob(req.body.Password));
    user.Password = undefined;
    // await userService.sendWelcomeEmail(user.Email,user.UserName)
    res.status(200).send(Constants.userRegisteredSuccess);
})

router.post('/login',async (req,res) => {

    let user = await userService.getUserByEmail(req.body.Email);
    if (!user) return res.status(400).send({ errorMessage: 'Invalid email or password.' });

    
    const validPassword = await bcrypt.compare(atob(req.body.Password), user.Password);
    if (!validPassword) return res.status(400).send({ errorMessage: 'Invalid email or password.' });

    user.Password = undefined;

    res.send({ User: user, _token: utilityService.generateAuthToken(user)});

})
module.exports = router;