const nodemailer = require('nodemailer');
const constants = require('./constants')

const transporter = nodemailer.createTransport({
    service: 'gmail',
    host: "smtp.gmail.com",
    port: 587,
    secure: true, // true for 465, false for other ports
    
    auth: {
        user: constants.emailServiceUserName,
        pass: constants.emailServicePassword
    }
});

module.exports = {
    sendEmail: async (mailOptions) => {
        await transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });
    }
}
