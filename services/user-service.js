const User = require('../models/User');
const bcrypt = require('bcrypt');
const emailService = require('../services/emailProvider-service')
const constants = require('../services/constants')
const Joi = require('joi');

const getEncryptedPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
}

module.exports = {

    
    userExists: async (email) => {
        return User.count({
            where: { Email: email }
        }) > 0;
    },

    registerUser: async (email, name, password) => {
        return await User.create({
            Email: email,
            Password: await getEncryptedPassword(password),
            Username: name,
        });
    },

    getUserByEmail: async (email) => {
        const userInfo = await User.findOne(
            { Email: email }
        )
        return userInfo
    },

    sendWelcomeEmail: async (email, userName) => {
        const staticContentTemplate = `<p style="font-size: 16px;">Dear ${userName}, Thank you for Signing up with us.</p><br>`
        const closingContentTemplate = `Thank you.<br/>`
        const finaltemplate = staticContentTemplate + closingContentTemplate;
        const mailOptions = {
            from: constants.emailServiceUserName,
            to: email,
            subject: 'Welcome User!!', // email subject
            html: finaltemplate
        };
        emailService.sendEmail(mailOptions)
    }
}