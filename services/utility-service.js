const Constants = require('./constants')
const jwt = require('jsonwebtoken');

module.exports = {
    getTokenExpirationDateTime: (minutes) => {
        const expirationDate = new Date(new Date().getTime() + minutes * 60000);
        return expirationDate.toISOString();
    },

    generateAuthToken: (user) => {
        return jwt.sign({ data: user.Username}, Constants.jwtPrivateKey, { expiresIn: '1h' });
    }
}